extends Resource

#export var worlds_ids:Array
export var worlds:Array
export var current_world_index:int = 0

func _init():
	if worlds.empty():
		# … load from file? todo
		# … meanwhile …
		worlds = [
			{
				'id': 2272006, 
				'title': "The Harvest by Gab'",
			},
			{
				'id': 999666,
				'title': "Strolling Hill",
			},
			{
				'id': 987654,
				'title': "The Pirate Bay",
			},
			{
				'id': 654321,
				'title': "The Coral",
			},
			{
				'id': 6, 
				'title': "Choisis ton nom by TadaStreaming",
			},
			{
				'id': 88, 
				'title': "Choisis ton nom by TadaStreaming",
			},
			{
				'id': 666999,
				'title': "Devil's Equation",
			},
			{
				'id': 1,
				'title': "The Octocave",
			},
			{
				'id': 291210,
				'title': "Canine Birth",
			},			
			{
				'id': 14,
				'title': "The simple Life by Gab'",
			},
			{
				'id': 7770777,
				'title': "Lucky Shenanigans",
			},
			{
				'id': 456789,
				'title': "Vulture",
			},
			{
				'id': 5551555,
				'title': "Abyss",
			},
			{
				'id': 444,
				'title': "Get dat Parkour",
			},
			{
				'id': 748596,
				'title': "The Ark",
			},
			{
				'id': 7,
				'title': "The Rock",
			},
			{
				'id': 3006, 
				'title': "The Hell Cloud by Gab'",
			},
			{
				'id': 123456,
				'title': "The Lion King",
			},
			{
				'id': 600002,
				'title': "Floating High",
			},
			{
				'id': 1001001,
				'title': "The Hermit",
			},
			{
				'id': 30, 
				'title': "Choisis un nom by Tadastreaming",
			},
			{
				'id': 43,
				'title': "The Tower",
			},
#			{
#				'id': 51,
#				'title': "lag",
#			},
			
#			{
#				'id': 3,
#				'title': "Boring",
#			},
			
#			{
#				'id': 1006,
#				'title': "Edgy Living",
#			},
		]
#		worlds_ids = [
#			666999,
#			999666,
#			666999818,
#			43,
#		]
		# L∞p
#		worlds_ids.append(worlds_ids.front())

func get_current_world():
	return self.worlds[self.current_world_index]

func get_current_world_id():
	return get_current_world()['id']

func get_current_world_title():
	var index = get_current_world()
	var value = index.title
	return value

func get_next_world():
	self.current_world_index = (self.current_world_index+1) % self.worlds.size()
	return get_current_world()

func get_next_world_id():
	return get_next_world()['id']
#	self.current_world_id = self.worlds_ids[(
#		self.worlds_ids.find(self.current_world_id) + 1
#		) % self.worlds_ids.size()]
#	return self.current_world_id
