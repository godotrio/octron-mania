# Octron Mania

A time-attack game made with Godot 3.2, Nakama 2.11 and Truncated Octahedrons, aka *Octrons*.

![A terrain made of Octrons with yellow, floating octrons.](promo/screenshot_00.png)
![A screenshot of Octron Mania.](promo/screenshot_01.png)

You can also go into "editor mode" and add and remove octrons with the mouse. (press F)

## How to play

- Download a build
    - Gitlab CI: TODO
    - mirror 1: 
- TODO: Itch
- TODO: Steam?
- TODO: Lutris
- TODO: …
- Play from source (see below)


## How to Install

0. Clone this repository
0. Download Godot 3.2 and run it
0. Open `project.godot` from Godot
0. Ignore the warnings (help!)
0. Press F5


## How to Contribute

Usual Git workflow.
Grab the source, hack around, make a merge request, sip some octron juice.

Contributions can take many shapes:
Promote the game, report a bug, suggest ideas, paint pixels, donate dough…


## FAQ

### Hey, leaderboards don't work!

Leaderboards require a key to connect, and that key is not provided with the source.

Look up `secrets.json.dist` and copy it to `secrets.json` to connect to your own Nakama instance.

The LUA modules are provided in the `nakama/` directory.



### Octrons ?

> Truncated Octahedrons are the hexagons of space. ([here's why](https://en.wikipedia.org/wiki/Permutohedron))

We believe they are important enough to warrant a mononymous name. Some suggested *Mecon*.
We call them *Octrons*, 'cause Mecon is sounding … weird, in french.  Sorry.

We want to see more of them in video games.
We have about sixteen thousand ideas of things to do with them.
