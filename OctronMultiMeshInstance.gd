extends MultiMeshInstance

# DEPRECATED ⋅ USE PLUING

var TruncatedOctahedronMesh = preload("res://addons/goutte.octron/octron_mesh.gd")

func _ready():
	# Create the multimesh.
	multimesh = MultiMesh.new()
	# Set the format first.
	multimesh.transform_format = MultiMesh.TRANSFORM_3D
	multimesh.color_format = MultiMesh.COLOR_NONE
	multimesh.custom_data_format = MultiMesh.CUSTOM_DATA_NONE
	# Then resize (otherwise, changing the format is not allowed).
	multimesh.instance_count = 100000
	# Maybe not all of them should be visible at first.
	multimesh.visible_instance_count = 50000
	# Set the transform of the instances.
#	for i in multimesh.visible_instance_count:
#		multimesh.set_instance_transform(
#			i,
#			Transform(Basis(), Vector3(i * 2 * 1.333, 0, 0))
#		)

	multimesh.mesh = TruncatedOctahedronMesh.new()
	
#	multimesh.mesh.export_shape("OctronShape.shape")

#	var hexagons_material = ShaderMaterial.new()
#	hexagons_material.shader = preload("res://octatron.shader")
#	hexagons_material.set_shader_param("my_texture", preload("res://assets/sprites/red-hex.png"))
#
#	var squares_material = ShaderMaterial.new()
#	squares_material.shader = preload("res://octatron.shader")
#	squares_material.set_shader_param("my_texture", preload("res://assets/sprites/red.png"))
#
#	var mesh_toc = MeshInstance.new()
#	mesh_toc.cast_shadow = true
#	mesh_toc.set_mesh(TruncatedOctahedron.new())
#
#	for i in range(0, 6): # first six are the squares, obviously
#		mesh_toc.set_surface_material(i, squares_material)
#	for i in range(6, 14):
#		mesh_toc.set_surface_material(i, hexagons_material)

var _instances_added = 0
func add_octron(where=Vector3()):
	#print("Adding octron at %s." % where)
	var i = _instances_added
	var wrong = Vector3(1.333, 0.933, 1.333)
	wrong = Vector3(1, 1, 1) # right
	multimesh.set_instance_transform(
		i,
		Transform(Basis(), where * wrong)
	)
	_instances_added += 1
