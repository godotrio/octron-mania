shader_type spatial;

render_mode unshaded;

varying vec3 pos;
varying vec3 axis;
varying float chaos;

uniform sampler2D my_texture;

void vertex() {

//	VERTEX = (MODELVIEW_MATRIX * vec4(VERTEX, 1.0)).xyz;
//	pos = (INV_CAMERA_MATRIX * vec4(VERTEX, 1.0)).xyz;
	pos = VERTEX;
	pos.x = pos.x + (1.0 - 1.0 * abs(sin(pos.x*0.5))) / (50000000.0 * pow(VERTEX.y - (sqrt(2) / 3.0), 2.0));

	axis = vec3(0, sqrt(2) / 3.0, 2.0 / 3.0);

//	chaos = randi(); // THAUR IS SAD

}

// http://docs.godotengine.org/en/3.0/tutorials/shading/shading_language.html#fragment-built-ins
void fragment() {
	vec3 v = VERTEX;
	float d = length(pos - axis);
//	float d2 = distance(pos, axis);
//
//	vec3 p = pos - axis;
//
//	bool is_in_moon = (d < 0.32 + (exp(pos.x) * 0.06) && d > 0.103 + (exp(pos.x) * 0.21));
//	bool is_in_circle = d < 0.03;
////	bool is_in_thing = int(round(d*40.0+sin(p.x)*3.0)) % int(sin(p.x)*11.0) == 0;
////	bool is_in_thing = int(round(d*41.0+sin(p.x)*3.0)) < int(round(d*42.0+sin(p.z)*3.0));
//	bool is_in_thing = int(round(d*41.0+sin(p.x)*13.0)) % 3 == 0;
////	bool is_in_other = abs(p.x * p.x) < (abs(p.z) * abs(p.x) * abs(p.y));
//	bool is_in_other = exp(p.x) == exp(p.y) + log(p.z);
//
//	float half_gap = 0.003;
//	if (
//		(is_in_moon && !is_in_other && !is_in_thing) ||
//		(is_in_circle) ||
//		(is_in_thing && !is_in_moon) ||
//		(is_in_other && !is_in_moon)
//	) {
//		ALBEDO.rgb = vec3(0.05,0.05,0.05);
////		ALBEDO.rgb = vec3(0.05,0.05,0.05);
//	}
	
	
	vec4 tex_color = texture(my_texture, UV);
	ALBEDO.rgb = ALBEDO.rgb * (1.0-tex_color.a) + tex_color.rgb * tex_color.a;
//	ALBEDO.rgb = vec3(UV.y,0.0,0.0);
	
	
	
}


