extends "res://addons/goutte.octron/octron_character.gd"


# The Character of Octron Mania


# Weirdly, this is not set during ready(),
# so we use find_node(), and that works.
# Probably something to do with inheritance.
onready var trail = $Trail3D



func ready():
	.ready()
	self.trail = find_node("Trail3D")
	enhance_trail()
	connect("terrain_collided", self, "restore_trail_perhaps")


func restore_trail_perhaps():
	if self.controls.get_ms_since_last_super_jump() > 444:
		restore_trail()


func restore_trail():
	self.trail.material_override.set_shader_param("color_alpha", 0.2)
	

func enhance_trail():
	self.trail.material_override.set_shader_param("color_alpha", 0.62)


## SOUND FX ####################################################################


func play_absorption_sound(orbital):
	var i = self.orbitals[orbital]
	var player:AudioStreamPlayer = get_node("AbsorptionSoundFx%02d" % [(i/5)%3])
	#player.stream.loop = false  # configure .import instead for OGG, or use WAV
	player.pitch_scale = 0.2 + 0.933 * (i / 14.0)
	player.play()
#	player.connect("finished", self, "stop_absorption_sound", [player])


################################################################################
