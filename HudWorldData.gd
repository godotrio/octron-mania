extends Control

onready var label_name = $Panel/LabelWorldName
onready var label_seed = $Panel/LabelWorldSeed
onready var label_music = $Panel/LabelWorldMusic

func set_world_data():
	
	set_world_seed()
	set_world_title()
	set_world_music()
	
	if !self.visible:
		self.set_visible(true)
	
func set_world_music():
	var value:String = MusicPlayer.get_current_track_data()["name"]
	set_label("music", value)

func set_world_seed():
	var value:String = str(god.game_state.get_current_world_id())
	set_label("seed",value)

#world names are stored in OctronManiaGameState.gd which is referenced in OctronManiaGame
func set_world_title():
	var value:String = god.game_state.get_current_world_title()
	set_label("title",value)
	pass

func set_label(data_type, value):
	if "seed" == data_type:
		label_seed.set_text("World Seed = %s" %[value])
	elif "music" == data_type:
		label_music.set_text("Music = %s" %[value])
	elif "title" == data_type:
		label_name.set_text("World = %s" %[value])
