extends "res://addons/goutte.octron/octron_world.gd"


export var goal_pickables_amount = 14
export var character_spawn_cell = Vector3(0, 77, 0)

var race_started_at  # ms
var goal_pickables_cells = Array()
var goal_pickables = Array()
var world_id = 666999  # the best, of course
var rng  # world random number generator, to be deterministic during biome gen


#func to_next_world():
#	get_tree().change_scene("res://TestOctronWorld.tscn")


func ready():
	.ready()
	App.connect('option_changed', self, '_on_app_option_changed')
	reposition_character()
	
	var best_score = god.score_manager.get_high_score(world_id)
	best_score = get_score_string(best_score)
	print("Previous best: %s" % best_score)
	update_best_score_hud()
	update_leaderboards_hud()


func get_world_color():
	return Color("#d5cb37")


func setup_camera():
	.setup_camera()
	self.camera.set_zfar(120)
	setup_camera_sensitivity()


func _on_app_option_changed(option_section, option_key, option_value):
	if "audio" == option_section and "camera_sensitivity" == option_key:
		setup_camera_sensitivity(option_value)


func setup_camera_sensitivity(camera_sensitivity=null):
	if null == camera_sensitivity:
		camera_sensitivity = App.get_option("controls", "camera_sensitivity", 100)
	self.camera.inertia_strength = camera_sensitivity * 0.01

func generate_world():
	
#	world_id = 666999818  # ok
#	world_id = 42  # lag :(
#	world_id = 43  # the tower escalade
#	world_id = 41  # bof
#	world_id = 666 # lag
#	world_id = 999 # nope
#	world_id = 111 # bof
#	world_id = 222 # lag
#	world_id = 333 # ok
#	world_id = 444 # ok parkour
#	world_id = 555 # ok
#	world_id = 777 # impossible?
#	world_id = 888 # lag
#	world_id = 1111 # lag
#	world_id = 1000 # tough
#	world_id = 1001 # impossible
#	world_id = 1002 # lag
#	world_id = 1003 # ok
#	world_id = 1004 # lag
#	world_id = 1005 # laggish
#	world_id = 1006 # ok
#	world_id = 2000 # no
#	world_id = 2020 # lag
#	world_id = 2022 # lag
#	world_id = 2023 # no
#	world_id = 2024 # lag
#	world_id = 2025 # lag
#	world_id = 2026 # ok
	
	rng = RandomNumberGenerator.new()
	rng.set_seed(world_id)
#	var my_random_number = rng.randf_range(-10.0, 10.0)

	var noise = OpenSimplexNoise.new()
	noise.seed = world_id
	noise.octaves = 4
	noise.period = 55.5
	noise.persistence = 0.8
	
	
	
	
	
	
	var all_octrons = OctronLattice.new()
	
	
	
	var CraftFixture = preload("res://addons/goutte.octron/fixtures/disk_fixture.gd")	
	
	var disk = CraftFixture.new()
	
	var floor_octron = Octron.new()
	floor_octron.color = Color('#00ff00')
	disk.octrons_models = [floor_octron]
	disk.fill_lattice(all_octrons)
	
	
	
	
	
	var n = 60  # radius of the disk
	var h = 30  # height of the world

	for xa in range(-n, n+1):
		for ya in range(h):
			for za in range(-n, n+1):
				if self.lattice.is_xyz_valid(xa, ya, za):
					# Cut out a cylinder
					if pow(xa,2) + pow(za,2) >= pow(n,2):
						continue
					var f = .440
					
					if ya > 1 and not noise.get_noise_3d(xa*f, za*f, ya*f) < -0.19:
						continue
					
					if ya <= 1: # AFTER get_noise_3d
						continue
					
					all_octrons.add_thing(Octron.new(), Vector3(xa, ya, za))
#					add_octron(Vector3(xa, ya, za))
	
	# Figure out pickables positions
	for _i in range(self.goal_pickables_amount):
		var cell = all_octrons.get_random_cell(rng)
		assert(cell)
		while all_octrons.has_thing_on(cell):
			cell += all_octrons.to_up()
		var cell_up = cell + all_octrons.to_up()
		if not all_octrons.has_thing_on(cell_up):
			cell = cell_up
		
		goal_pickables_cells.append(cell)
	
	var count = all_octrons.size()
	
	print("Generated %d terrain octrons." % count)
	
	var curated_count = 0
	for cell in all_octrons.get_all_cells():
		if not all_octrons.has_14_neighbors(cell):
			add_octron(all_octrons.get_thing(cell), cell)
			curated_count += 1
	
	print("Added %d terrain octrons. (-%.0f%%)" % [curated_count, 100 * (count - curated_count) / count])
	
	generate_pickables()

	
func generate_pickables():
	
	if not goal_pickables.empty():
		for pickable in goal_pickables:
			pickable.destroy()
		goal_pickables.clear()
	
	for i in range(self.goal_pickables_amount):
		
		var OctronPickableScene = preload("res://PickableOctron.tscn")
		var op = OctronPickableScene.instance()
		op.set_name("OctronPickable%02d" % i)
		op.color = get_world_color()
		var cell = goal_pickables_cells[i]
		lattice.move_node_to_cell(op, cell)
		# DEBUG
#		lattice.move_node_to_cell(op, Vector3(0, 3, i*2))
		
		add_child(op)
		
		op.get_node("Area").connect("body_entered", self, "_on_pickable_entered", [op])
		goal_pickables.append(op)
	
	print("Generated %d pickables." % [self.goal_pickables_amount])


#func setup_characters():
#	.setup_characters()

func after_world_generation():
	.after_world_generation()
	HudWorldData.set_world_data()
	start_race()


func setup_characters():
	.setup_characters()
	self.character.can_jump_outline_color = get_world_color()


func reset_timer():
	race_started_at = get_current_time()


func start_race():
	reset_timer()
	start_timer()
	MusicPlayer.play_next_track()


func get_current_time():
	return OS.get_ticks_msec()


func _on_pickable_entered(body, pickable):
	#print("Pickable `%s' entered by `%s'." % [pickable.name, body.name])
	
	if body is OctronCharacter:
		goal_pickables.remove(goal_pickables.find(pickable))
#		pickable.queue_free()
		pickable.get_node("Area").call_deferred('set_monitoring', false)
		pickable.follow(body)
		pickable.animate_pickup()
		self.character.add_orbital(pickable)
		check_victory_conditions()

var _timer
var has_finished = false
func check_victory_conditions():
	if not goal_pickables.empty():
		return
	# We won ! YAY
	has_finished = true
	pause_timer()
	update_timer_hud()
	print("VICTORY!")
	var score = get_current_score()
	var score_string = get_score_string(score)
	print("Your time : %s" % [score_string])
	
	var is_best = god.score_manager.submit_score(score, world_id)
	if is_best:
		print("== NEW BEST TIME! ==")
		$BestScorePanel/NewBestParticles.restart()
		submit_score_to_leaderboard(score)
		#update_leaderboards_hud()
	
	update_best_score_hud()
	
	for orbital in character.orbitals:
		orbital.animate_absorb()
	
	start_absorption()
#	yield(get_tree().create_timer(6.0), "timeout")
#	stop_absorption()
	_timer = get_tree().create_timer(6.0)
	_timer.connect("timeout", self, "stop_absorption")
	
	
	if is_best:
		yield(get_tree().create_timer(3.0), "timeout")
		update_leaderboards_hud()

func clear_movement_inputs():
	self.character.controls.clear_movement_inputs()


var _gravity_scale_cache = 0.0

func start_absorption():
	clear_movement_inputs()
	self.should_process_inputs = false
	_gravity_scale_cache = self.character.gravity_scale
	self.character.gravity_scale = 0.0
	self.character.apply_gravity_magic = false
	self._is_character_absorbing = true
	self._character_absorption_position = self.character.translation + Vector3(0, 33-self.character.translation.y, 0)


func stop_absorption():
	self.should_process_inputs = true
	self.character.gravity_scale = _gravity_scale_cache
	self.character.apply_gravity_magic = true
	self._is_character_absorbing = false
	self._character_absorption_position = null
	

#onready var score_hud = $TimeScorePanel/TimeScoreLabel


var _current_score
func get_current_score():  # ms
	if _current_score:
		return _current_score
	var score = get_current_time() - self.race_started_at
	return score


func get_current_score_s_string():
	return get_score_s_string(get_current_score())


func get_current_score_ms_string():
	return get_score_ms_string(get_current_score())


func get_score_string(score):
	return get_score_s_string(score) + get_score_ms_string(score)


func get_score_s_string(score):
	if score == INF:
		return "--:--"
	score = int(score / 1000.0)
	return "%02d:%02d" % [min(99,score / 60), score % 60]


func get_score_ms_string(score):
	if score == INF:
		return ".--"
	score = int(round(score/10.0)) % 100
	return ".%02d" % [score]


func update_best_score_hud():
	var best = god.score_manager.get_high_score(world_id)
	var best_score_panel = $BestScorePanel
	var best_score_label = $BestScorePanel/BestScoreLabel
	if INF == best:
		best_score_panel.set_visible(false)
	else:
		best_score_panel.set_visible(true)
		best_score_label.set_text(get_score_string(best))


func update_timer_hud():
	var score_hud_s = $TimeScorePanel/TimeScoreLabel
	var score_hud_ms = $TimeScorePanel/TimeScoreMsLabel
	if is_inside_tree() and score_hud_s and score_hud_ms:
		score_hud_s.set_text(get_current_score_s_string())
		score_hud_ms.set_text(get_current_score_ms_string())


var _timer_paused = false
func pause_timer():
	_timer_paused = true
	_current_score = get_current_score()


func start_timer():
	_timer_paused = false
	_current_score = null
	

var _is_character_absorbing = false
var _character_absorption_position
func process(delta):
	.process(delta)
	if not _timer_paused:
		update_timer_hud()
	process_infinite_bottom()
	
	
	if _is_character_absorbing:
		_character_absorption_position += Vector3(0, 0.0155, 0)
#		print("add_central_force")
#		self.character.add_central_force((_character_absorption_position - self.character.translation) * 0.16)
#		self.character.apply_central_impulse((_character_absorption_position - self.character.translation) * 0.16)
		self.character.translation += (_character_absorption_position - self.character.translation) * 0.02
		self.character.rotation *= 0.969696
#		self.character.


const OUT_OF_BOUNDS = 150
func process_infinite_bottom():
	for character in self.characters:
		if character.translation.y < -OUT_OF_BOUNDS:
			
			if has_finished:
				go_to_next_world()
			
			var previous_character_position = character.translation + Vector3()
			character.translation = Vector3(0, OUT_OF_BOUNDS, 0)
			character.eye.translation = character.get_eye_position()
			
			var diff = self.character.translation - previous_character_position
			for orbital in self.character.orbitals:
				orbital.translation = orbital.translation + diff
				orbital.trail.translate_trail(diff)
#				prints("Teleported orbital %s to " % orbital.name, orbital.translation)
			
			self.character.trail.translate_trail(diff)


func go_to_next_world():
	show_loading_screen()
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	god.go_to_next_world()


func process_other_inputs(event):
	.process_other_inputs(event)
	if Input.is_action_just_pressed("reset_level"):
		reset_level()
	if Input.is_action_just_pressed("toggle_mouse_mode"):
		self.camera.toggle_mouse_mode()
	if Input.is_action_just_pressed("toggle_leaderboards"):
		$LeaderboardWidget.visible = not $LeaderboardWidget.visible
	if Input.is_action_just_pressed("toggle_hud"):
		toggle_hud()
	if Input.is_action_just_pressed("toggle_game_menu"):
		self.camera.toggle_mouse_mode()
		$GameMenu.visible = not $GameMenu.visible


func reset_character():
	reposition_character()
	for orbital in self.character.orbitals.duplicate():
		orbital.destroy()
#		self.character.remove_orbital(orbital)


func reposition_character():
	var target_character = self.character
	target_character.translation = self.character_spawn_cell
	target_character.rotation = Vector3()
#	clear_movement_inputs()


func reset_level():
	has_finished = false
	generate_pickables()
#	reset_timer()
	reset_character()
	clear_movement_inputs()
	HudWorldData.set_world_data()
	start_race()
#	get_tree().reload_current_scene()


func show_loading_screen():
	$LoadingScene.set_visible(true)
	### Won't effect if put here…
#	yield(get_tree(), "idle_frame")
#	yield(get_tree(), "idle_frame")
	###

var _hud_shown = true
func toggle_hud():
	_hud_shown = not _hud_shown
	HudWorldData.visible = _hud_shown
	$LeaderboardWidget.visible = _hud_shown

#const ConfigFiles = preload("res://addons/goutte.configfiles/config_files.gd")

func update_leaderboards_hud():
	var nakama_client = god.get_nakama_client()
#	var nakama_session = god.get_nakama_session()  # there be MT dragons
	var nakama_session = yield(
		nakama_client.authenticate_device_async(
			get_unique_user_id()
		),
		"completed"
	)
	
	assert(nakama_client)
	assert(nakama_session)
	var leaderboard_id = get_leaderboard_id()
	var leaderboard_records = yield(
		nakama_client.list_leaderboard_records_async(
			nakama_session,
			leaderboard_id,
			null,
			null,
			10
		),
		"completed"
	)
	print("Leaderboards Records")
	print(leaderboard_records)
	
	$LeaderboardWidget.update_with_nakama_leaderboard(leaderboard_records)


func submit_score_to_leaderboard(score:int):
	var nakama_client = god.get_nakama_client()
#	var nakama_session = god.get_nakama_session()
	var nakama_session = yield(
		nakama_client.authenticate_device_async(
			get_unique_user_id()
		),
		"completed"
	)
	
	var leaderboard_id = get_leaderboard_id()
	var submission = yield(
		nakama_client.write_leaderboard_record_async(
			nakama_session,
			leaderboard_id,
			score
		),
		"completed"
	)
	print("Leaderboard Submission:")
	print(submission)


func get_leaderboard_id():
	return "octron-mania-%d" % world_id


func get_unique_user_id():
	# This is not supported by Godot in HTML5, use a different way to generate an id, or a different authentication option.
	return App.get_unique_device_id()
#	return OS.get_unique_id()

