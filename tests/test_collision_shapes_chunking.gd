extends "res://addons/gut/test.gd"

var _started_at

func start_chrono():
	_started_at = OS.get_system_time_msecs()
#	print("Starting chronometer…")

func stop_chrono():
	var elapsed = OS.get_system_time_msecs() - _started_at
#	print("That took %dms." % elapsed)
	return elapsed

func before_each():
	gut.p("before_each()", 2)

func after_each():
	gut.p("after_each()", 2)

func before_all():
	gut.p("before_all()", 2)

func after_all():
	gut.p("after_all()", 2)

#func test_assert_eq_number_not_equal():
#	assert_eq(1, 2, "Should fail.  1 != 2")

func test_chunking_10():
	var times = Array()
	for i in range(101):
		start_chrono()
		
		var sb = create_static_body()
		for k in range(i):
			var cs = create_collision_shape()
			sb.add_child(cs)
		
		var time = stop_chrono()
		times.append(time)
	
	print('RESULTS')
	print('shapes_amount, total_time_ms, time_per_shape_ms')
	for i in range(1, times.size()):
		print("%d, %d, %.3f" % [i, times[i], times[i] / (i*1.0)])
	

func test_euclidean_div():
	var s = 3
	for n in range(-15, 15):
		n = n - s + 1 if n < 0 else n
		prints(n, n / s)

#func test_chunking_100():
#	var sb = create_static_body()
#	for i in range(100):
#		var cs = create_collision_shape()
#		sb.add_child(cs)


func create_static_body():
	var sb = StaticBody.new()
	add_child(sb)
	return sb

func create_collision_shape():
	var cs = CollisionShape.new()
	cs.shape = preload("res://addons/goutte.octron/octron.shape")
	return cs
