shader_type spatial;
render_mode unshaded;

// Another Rainbow shader.  Such a classic!

uniform vec4 base_color : hint_color = vec4(1.0);
//uniform vec4 contour_color = ColorN("azure", _alpha);

//uniform vec4 color_00 : hint_color = vec4(1.0);

uniform float color_alpha = 1.0;
uniform float movement_speed = 0.01;

uniform vec3 target_position = vec3(0.0);

uniform bool use_texture = false;
uniform sampler2D flow_texture :hint_albedo;

uniform bool use_outline = false;
uniform vec4 outline_color : hint_color = vec4(1.0);
uniform float outline_width = 0.1;

//varying vec3 model_vertex;
//
//void vertex() {
//	model_vertex = VERTEX;
//}

//UV.x is trail length.(0= octron pos, 1: end of trail)
//UV.y is trail breadth(0 to 1)


void fragment() {
	
	if (use_texture) {
		vec2 _uv = UV + vec2(TIME*2.15, 0);
//		vec2 _uv = UV + vec2(TIME*movement_speed, 0);
		vec4 tex = texture(flow_texture, _uv);
		ALBEDO = tex.rgb;
//		ALPHA = tex.a;
		ALPHA = tex.a * color_alpha;
	
		if (use_outline) {
			if (_uv.y > (1.0-outline_width) || _uv.y < outline_width) {
				ALBEDO = outline_color.rgb;
			}
		}
		
	} else {
		
		
	
		// Square rainbows
	//	bool is_thing = sin(UV.x * 6.28 * 15.0) > -0.618; 
	//	float _alpha = color_alpha;
	//	if (is_thing) {
	//		_alpha = 0.0;
	//	}
	//	ALBEDO = base_color.rgb * COLOR.rgb;
	//	ALPHA = _alpha;
		
		
		float t = TIME * movement_speed * 100.0;
		bool is_thing = sin(t + UV.x * 6.28 * 15.0) > 0.0; 
	//	bool is_thing2 = sin(t + UV.x * 6.28 * 15.0) < 0.0; 
		
		float _alpha = color_alpha;
		
	//Trail is segmented in two parts, X axis
	//Part 1
		if (is_thing) {
				_alpha = 0.5;
				ALBEDO = base_color.rgb;
				ALPHA = _alpha;
			//Altering shader on Y axis
			if (UV.y > 0.80 || UV.y < 0.2){
				_alpha = 0.5;
				ALPHA = _alpha;
				ALBEDO = base_color.rgb * COLOR.rgb;
				}
		}
				
				
	//Part 2
		
		if (!is_thing) {
			_alpha = 0.5;
			ALBEDO = base_color.rgb * COLOR.rgb;
			ALPHA = _alpha;
			if (UV.y > 0.80 || UV.y < 0.2){
				_alpha = 0.5;
				ALPHA = _alpha;
				ALBEDO = base_color.rgb;
				}
			}
			
		
	//	if (UV.y > 0.80 || UV.y < 0.2)
	//		_alpha = 1.0;
	//		ALPHA = _alpha;
	//		ALBEDO = base_color.rgb;
	//		if (UV.y > 0.80 || UV.y < 0.2)
	//			ALBEDO = base_color.rgb;
	//			_alpha = 1.0;
	//			ALPHA = _alpha;
			
	
	//	if (is_thing2) {
	//		ALBEDO = base_color.rgb;
	//		_alpha = 1.0;
	//	}
	//	EMISSION = base_color.rgb;
	//	if (UV.x)
		
	//	if (UV.y > 0.80 || UV.y < 0.2)
	//		_alpha = 1.0;
	//		ALBEDO = base_color.rgb * COLOR.rgb;
	
	}
}

//orbital_trail_rainbow_white_segmented_bordered_full_segmented

//	if (is_thing) {
//			_alpha = 0.5;
//			ALBEDO = base_color.rgb;
//			ALPHA = _alpha;
//		//Altering shader on Y axis
//		if (UV.y > 0.80 || UV.y < 0.2){
//			_alpha = 1.0;
//			ALPHA = _alpha;
//			ALBEDO = base_color.rgb * COLOR.rgb;
//			}
//	}
//
//
////Part 2
//
//	if (!is_thing) {
//		_alpha = 0.5;
//		ALBEDO = base_color.rgb * COLOR.rgb;
//		ALPHA = _alpha;
//		if (UV.y > 0.80 || UV.y < 0.2){
//			_alpha = 1.0;
//			ALPHA = _alpha;
//			ALBEDO = base_color.rgb;
//			}
//		}