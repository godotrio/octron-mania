shader_type spatial;
render_mode unshaded, cull_front;


// Simple outline shader.
// Use in a ShaderMaterial, as second pass.


uniform bool enabled = true;
uniform float thickness = 0.05; // as a fraction of the object
uniform vec4 color : hint_color = vec4(0.0, 0.0, 0.0, 1.0);


void vertex() {
	if (enabled) {
		VERTEX *= 1.0 + thickness;
	}
}


void fragment() {
	if (enabled) {
		ALBEDO = color.rgb;
	}
}