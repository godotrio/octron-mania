
var score_filepath:String = "user://octron_mania_scores.json"

# Contents of the JSON file above
var scores:Dictionary = Dictionary()

# For time attacks and such
var _low_is_best:bool = false


func _init(low_is_best:bool=false):
	self._low_is_best = low_is_best
	create_file_if_missing()
	
	var file:File = File.new()
	file.open(score_filepath, File.READ)
	var content = file.get_as_text()
	file.close()
	
	var json = JSON.parse(content)
	if not (json.error == OK):
		# Note that the invalid JSON file will be overwritten
		printerr(
			"Cannot parse JSON score file:\n"+
			"%s\nLine %d" % [json.error_string, json.error_line])
		return
	self.scores = json.result
	assert(self.scores is Dictionary)


func _hash(thing):
	# Parsing Dicts from JSON always yield string keys, it appears.
	# To avoid collisions, let's make sure we always use strings.
	return str(thing)


func create_file_if_missing():
	var file:File = File.new()
	if not file.file_exists(score_filepath):
		write_to_file(Dictionary())


func write_to_file(something_serializable):
	var file:File = File.new()
	file.open(score_filepath, File.WRITE)
	file.store_string(JSON.print(something_serializable))
	file.close()


func commit_to_file():
	write_to_file(self.scores)


func submit_score(score, for_thing):
	var is_high = false
	if is_high_score(score, for_thing):
		is_high = true
		set_high_score(score, for_thing)
	return is_high


func is_high_score(score, for_thing):
	var high_score = get_high_score(for_thing)
	if self._low_is_best:
		return score < high_score
	else:
		return score > high_score


func has_high_score(for_thing):
	return self.scores.has(_hash(for_thing))


func set_high_score(score, for_thing):
	self.scores[_hash(for_thing)] = score
	commit_to_file()  # Thread me!


func get_high_score(for_thing):
	if not has_high_score(for_thing):
		return INF * (1 if self._low_is_best else -1)
	return self.scores[_hash(for_thing)]


#func clear_scores()

