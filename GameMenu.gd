extends Control

func _ready():
	$BackgroundPanel/VBoxContainer/CameraSensitivityHSlider.value = \
		App.get_option('controls', 'camera_sensitivity', 100)
	
	var nakama_client = god.get_nakama_client()
#	var nakama_session = god.get_nakama_session()
	var nakama_session = yield(
		nakama_client.authenticate_device_async(
			App.get_unique_device_id()
		),
		"completed"
	)
#	prints("NAKAMA SESSION", nakama_session)
	
	if not nakama_session.is_exception():
	
		var account = yield(
			nakama_client.get_account_async(nakama_session),
			"completed"
		)
#		print("Nakama User Account")
		#print(account)
		#> custom_id: Null, devices: [id: cc82178c62164ab98d04f411f3694ebd, vars: [], ], email: Null, user: avatar_url: Null, create_time: 2020-03-08T18:41:54Z, display_name: Null, edge_count: Null, facebook_id: Null, gamecenter_id: Null, google_id: Null, id: 4c4af2ed-afb2-4f1c-bf95-125090f525b4, lang_tag: en, location: Null, metadata: {}, online: Null, steam_id: Null, timezone: Null, update_time: 2020-03-08T18:41:54Z, username: xhhJpqmkxF, , verify_time: Null, wallet: {},
		
		if account is NakamaException:
			printerr("Cannot fetch nakama account!")
			printerr(account)
		else:
			var username = account.user.username
#			print("USERNAME")
#			print(username)
			$BackgroundPanel/VBoxContainer/UsernameLineEdit.text = username


func _on_QuitButton_pressed():
	App.quit()


func _on_CameraSensitivityHSlider_value_changed(value):
	App.set_option('controls', 'camera_sensitivity', value)


func _on_MainVolumeHSlider_value_changed(value):
	App.set_option('audio', 'main_volume', value)
	App.adjust_main_volume_from_options()  # ideally, listen to signal instead


func _on_ResumeButton_pressed():
	self.visible = false
	get_viewport().get_camera().toggle_mouse_mode()
#	(get_viewport().get_camera() as TrackballCamera).toggle_mouse_mode()


func _on_UsernameLineEdit_focus_exited():
	pass # Replace with function body.
	_on_UsernameLineEdit_text_entered($BackgroundPanel/VBoxContainer/UsernameLineEdit.text)


func _on_UsernameLineEdit_text_entered(new_username):
	print("Saving Username…")
	
	var nakama_client = god.get_nakama_client()
#	var nakama_session = god.get_nakama_session()
	var nakama_session = yield(
		nakama_client.authenticate_device_async(
			App.get_unique_device_id()
		),
		"completed"
	)
	
	var account = yield(
		nakama_client.update_account_async(
			nakama_session,
			new_username,
			new_username
		),
		"completed"
	)
	
	prints("Nakama.update_account response :", account)
	


const REPO = "https://framagit.org/godotrio/octron-mania"


func _on_ContributeCodeButton_pressed():
	OS.shell_open(REPO)


func _on_ContributeBugsButton_pressed():
	OS.shell_open("%s/issues" % REPO)


func _on_ContributeIdeasButton_pressed():
	OS.shell_open("%s/issues" % REPO)


