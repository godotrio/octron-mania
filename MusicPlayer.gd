extends AudioStreamPlayer


#Music player SINGLETON, handle music playlist.
#Stage music is randomly determined
#Main menu music is only loaded at the creation of node.


# We load all the OGG tracks from this directory
var stage_tracks_path = "assets/music/stage"

var playlist:Array = Array()


func _ready():
	
	create_playlist_from_dir()


func get_title_from_audio_file(file_path):
	var file_name:String = Array(file_path.split("/")).back()
	var dot_pos = file_name.find_last('.')
	if dot_pos > 0:
		file_name = file_name.substr(0, dot_pos)
	return file_name


func create_playlist_from_dir():
	
	# Niiiiique! :3
	var files = Finder.list_files_with_extensions(stage_tracks_path, [
		'.ogg', '.mp3', '.wav',
	])
	
	for file in files:
		playlist.append({
			"name": get_title_from_audio_file(file),
			"path": file,
		})
	
	assert(not playlist.empty())
	
	randomize()
	playlist.shuffle()
	print("Added %d tracks to the playlist." % [playlist.size()])


var _current_track_id := 0
func play_next_track():
	
	var track = playlist[_current_track_id]
	if not track:
		printerr("No music track found.")
		return
	
	print("Loading track #%02d: `%s' from `%s'." % [
		_current_track_id, track.name, track.path,
	])
	set_stream(load(track.path))
	play()
#	set_current_track_data()
	_current_track_id = (_current_track_id + 1) % playlist.size()

#var track_data:Dictionary
#func set_current_track_data():
#	track_data.name = playlist[_current_track_id].name

func get_current_track_data():
	
	return playlist[_current_track_id]
