extends Resource
class_name OctronFixture


# A bunch of Octrons in a lattice with a stack size of 1.
# A base (astract) class to extend.
# You should override craft() and add your own logic,
# using add_octron(0, on_cell)
# 


var lattice:OctronLattice
var octrons_models = Array()


func _init():
	init()


func init():
	init_lattice()
	init_models()


func init_lattice():
	self.lattice = OctronLattice.new()


func init_models():
	self.octrons_models.append(Octron.new())


func set_octrons_models(octrons):
	self.octrons_models = octrons


func get_octron_model(idx:int):
	return self.octrons_models[idx]


#func merge_in_lattice(lattice, on_cell):
func fill_lattice(lattice, on_cell=Vector3.ZERO):
	craft_if_needed()
	for local_cell in self.lattice.get_all_cells():
		lattice.add_thing(
			self.lattice.get_thing_on_cell(local_cell),
			on_cell + local_cell
		)


var _has_crafted = false


func craft_if_needed():
	if not _has_crafted:
		_has_crafted = true
		craft()



func craft():
#	add_octron()
	pass


#func fill_cube(c):
#	pass


func instance_octron(octron_idx):
	return get_octron_model(octron_idx).duplicate(true)


func add_octron(octron_idx, on_cell) -> void:
	var octron = instance_octron(octron_idx)
	add_octron_instance(octron, on_cell)


func add_octron_instance(octron, on_cell) -> void:
	assert(self.lattice)
	self.lattice.add_thing(octron, on_cell)




