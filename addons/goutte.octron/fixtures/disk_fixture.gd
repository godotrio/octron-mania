extends OctronFixture


func craft():
	
	# Flat disk
	var n = 60  # radius of the disk
	var h = 1  # height of the disk

	var count = 0
	var all_octrons = OctronLattice.new()

	for xa in range(-n, n+1):
		for ya in range(h):
			for za in range(-n, n+1):
				if self.lattice.is_xyz_valid(xa, ya, za):
					# Cut out a cylinder
					if pow(xa,2) + pow(za,2) >= pow(n,2):
						continue
					
					add_octron(0, Vector3(xa, ya, za))


