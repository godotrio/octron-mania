extends Spatial

# Handles collision shapes for terrain octrons.
# Extra-chunky, like peanut butter.
# 
#
# Rationale
# 
# We want to be able to "bump" into each of the terrain Octrons.
# We want to select Octrons with the mouse or a cursor.
# One Static Body per terrain Octron yields a lot (too many) nodes.
# Perf suffers.
# Static Bodies support multiple Collision, but… see figure_c.png
# So, we chunk.  Initial tests are promising.
# Insights on ideal chunk size are welcome (and much needed).
# 

const OctronLattice = preload("res://addons/goutte.octron/octron_lattice.gd")
const OctronStaticBodyChunk = preload("res://addons/goutte.octron/octron_static_body_chunk.gd")


# Careful, the total amount of octrons is not x * y * z
export(int) var chunk_size_x : int = 16
export(int) var chunk_size_y : int = 16
export(int) var chunk_size_z : int = 16


var lattice = OctronLattice.new()


# Chunk Coordinates (Vector3) => OctronStaticBodyChunk
var _chunks = Dictionary()



func _init():
	assert(self.chunk_size_x > 0)
	assert(self.chunk_size_y > 0)
	assert(self.chunk_size_z > 0)


func add_octron(where):
	# Find the chunk coordinates
	var chunk_coords = get_chunk_coordinates(where)
	# Find the chunk, and if there is no chunk, create it
	var chunk = get_or_create_chunk_at(chunk_coords)
	# Add a collision shape to the chunk
	chunk.add_octron(where)


func remove_octron(where):
	# Find the chunk coordinates
	var chunk_coords = get_chunk_coordinates(where)
	# Find the chunk
	var chunk = get_chunk_at(chunk_coords)
	# and if there is no chunk, GTFO
	if not chunk:
		printerr("[%s] Cannot remove: no chunk for cell `%s'." % [name, where])
		return
	# Remove the collision shape to the chunk
	chunk.remove_octron(where)


func find_cell(chunk, shape_id): # -> Vector3?
	if not has_chunk(chunk):
		print("[%s] Can't find cell under chunk:" % name)
		print(chunk)
		return null
	return chunk.find_cell(shape_id)


func get_chunk_size() -> Vector3:
	return Vector3(self.chunk_size_x, self.chunk_size_y, self.chunk_size_z)


func get_chunk_coordinates(where:Vector3) -> Vector3:
	# Euclidean divisions
	var x = int((where.x - self.chunk_size_x + 1) if where.x < 0 else where.x) / self.chunk_size_x
	var y = int((where.y - self.chunk_size_y + 1) if where.y < 0 else where.y) / self.chunk_size_y
	var z = int((where.z - self.chunk_size_z + 1) if where.z < 0 else where.z) / self.chunk_size_z

	return Vector3(x, y, z)


func get_or_create_chunk_at(chunk_coords:Vector3) -> OctronStaticBodyChunk:
	if has_chunk_at(chunk_coords):
		return get_chunk_at(chunk_coords)
	else:
		return create_chunk_at(chunk_coords)


func has_chunk(chunk:OctronStaticBodyChunk) -> bool:
	return self._chunks.values().has(chunk)


func has_chunk_at(chunk_coords:Vector3) -> bool:
	return self._chunks.has(chunk_coords)


func get_chunk_at(chunk_coords:Vector3) -> OctronStaticBodyChunk:
	if not has_chunk_at(chunk_coords):
		return null
	return self._chunks[chunk_coords]


func create_chunk_at(chunk_coords:Vector3) -> OctronStaticBodyChunk:
	var chunk_size = get_chunk_size()
	var chunk = OctronStaticBodyChunk.new(chunk_coords, chunk_size)
	self._chunks[chunk_coords] = chunk
	chunk.set_name("%s(%03d,%03d,%03d)" % [
		get_name(),
		chunk_coords.x,
		chunk_coords.y,
		chunk_coords.z,
	])
	chunk.set_translation(chunk_coords * chunk_size)
	add_child(chunk)
	return chunk

