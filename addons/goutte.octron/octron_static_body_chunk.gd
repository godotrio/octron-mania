extends StaticBody


const OctronShape = preload("res://addons/goutte.octron/octron.shape")


var _chunk_coords:Vector3
var _chunk_size:Vector3


var _collider_to_cell := Dictionary()  # CollisionShape => Vector3
var _cell_to_collider := Dictionary()  # Vector3 => CollisionShape


func _init(chunk_coords:Vector3, chunk_size:Vector3):
	self._chunk_coords = chunk_coords
	self._chunk_size = chunk_size


func add_octron(where:Vector3):
	var cs = CollisionShape.new()
	cs.set_shape(OctronShape)
	cs.set_name("OctronCollisionShape(%05d,%05d,%05d)" % [
		where.x, where.y, where.z,
	])
	cs.set_translation(where - get_translation())
	self._collider_to_cell[cs] = where
	self._cell_to_collider[where] = cs
	add_child(cs)


func remove_octron(where:Vector3):
	if not self._cell_to_collider.has(where):
		printerr("[%s] Nothing to remove at `%s'." % [name, where])
		return
	var cs = self._cell_to_collider[where]
	
	if self._collider_to_cell.has(cs):
		self._collider_to_cell.erase(cs)
	self._cell_to_collider.erase(where)
	
	cs.queue_free()


func find_cell(shape_index:int):
	var shape_owner_id = shape_find_owner(shape_index)
	var shape_owner = shape_owner_get_owner(shape_owner_id)
	
	if not self._collider_to_cell.has(shape_owner):
		printerr("[%s] No cell found for `%s'." % [name, str(shape_index)])
		return null
	
	return self._collider_to_cell[shape_owner]


