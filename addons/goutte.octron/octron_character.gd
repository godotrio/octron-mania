#extends KinematicBody
# It's just immediately more fun with the physics of
extends RigidBody
class_name OctronCharacter

# A base class for your Character to extend?


#const OctronMesh = preload("res://addons/goutte.octron/octron_mesh.gd")
const OctronLattice = preload("res://addons/goutte.octron/octron_lattice.gd")
const OctronStaticBodyChunk = preload("res://addons/goutte.octron/octron_static_body_chunk.gd")


export var orbit_radius = 1.7
export var orbitals_speed = 0.333  # turns/s


var can_jump_outline_color = Color(0,1,0,1)
var default_outline_color = Color(0,0,0,1)


onready var shape = $CollisionShape
onready var mesh = $MeshInstance


var controls : OctronCharacterControls

var dynamic_orbitals_amplitude := 1.0
var orbital_time := 0.0


var _gravity_scale := 1.0
func _ready():
	ready()
	_gravity_scale = gravity_scale


func _process(delta):
	reposition_eye()
	increment_orbitals_time(delta)
	if controls.can_jump():
		set_outline_color(can_jump_outline_color)
	else:
		set_outline_color(default_outline_color)


var fall_gravity_multiplier = 1.212
var apply_gravity_magic = true

func _physics_process(delta):
	pass
#	if apply_gravity_magic:
#		if is_falling():
#			gravity_scale = _gravity_scale * fall_gravity_multiplier
#		else:
#			gravity_scale = _gravity_scale
			


func _exit_tree():
	# When the node exits the Scene Tree, this function is called.
	# Children nodes have all exited the Scene Tree at this point
	# and all became inactive.
	macronize_eye()


func ready():
#	generate_collision_shape()
#	generate_mesh()
	generate_eye()
	tweak_physics()

func set_controls(controls):
	self.controls = controls

func process_controls(delta):
	if null != self.controls:
		self.controls.process_controls(delta)

func is_falling():
	return self.linear_velocity.dot(Vector3.DOWN) > 0.01
		

################################################################################
## PHYSICS #####################################################################

# These defaults belong in the scene.

func tweak_physics():
	var pm = PhysicsMaterial.new()
	pm.bounce = 0.0
#	pm.friction = 0.555
	pm.friction = 0.777
	self.physics_material_override = pm


func become_bouncy():
	self.physics_material_override.bounce = 0.618


func stop_being_bouncy():
	self.physics_material_override.bounce = 0.0


################################################################################
## EYE #########################################################################

# Eye to use as anchor for the camera.
# It follows the character but does not rotate with it.
# Its position is maintained above the character. (cf. eye_relative_position)
var eye:Spatial

export var eye_relative_position := Vector3(0.0, 1.0, 0.0)
# Whether the relative position above is at the scale of the character.
export var eye_relative_position_scaled := true


func generate_eye():
	self.eye = Spatial.new()
	self.eye.name = "%sEye" % name
	get_parent().add_child(self.eye)


func macronize_eye():
	# Gut the eye out with weapons of war.
	self.eye.call_deferred("queue_free")
	# Note that this will probably delete the camera as well.
	# … Just like in real life !


func reposition_eye():
	self.eye.translation = get_eye_position()


func get_eye_position():
	var destination = to_global(Vector3(0, 0, 0))
	if self.eye_relative_position_scaled:
		destination += self.eye_relative_position * self.scale
	else:
		destination += self.eye_relative_position
	
	return destination


################################################################################


var orbitals = Dictionary()
#var orbitals = Array()  # hmmm…  which is faster for us?

func add_orbital(spatial_node):
	assert(not orbitals.has(spatial_node))
	self.orbitals[spatial_node] = self.orbitals.size()
	
	#colorize_surface_for(spatial_node, ColorN('chartreuse'))


func remove_orbital(orbital):
	self.orbitals.erase(orbital)


func increment_orbitals_time(delta):
	orbital_time += delta * orbitals_speed * dynamic_orbitals_speed * TAU


func get_orbit_of(orbital, orbital_speed=1.0):
#	var time = (orbital_time * 0.001) * orbitals_speed * orbital_speed * dynamic_orbitals_speed * TAU
	var time = orbital_time
	var os = self.orbitals.size()
	var theta = self.orbitals[orbital] * TAU / os
	var y_amplitude = orbital.oscillation_amplitude
#	var amplitude = dynamic_orbitals_amplitude
	var curve = Vector3(
		sin(theta + time),
		y_amplitude * sin(theta + time * orbital.oscillation_speed),
		cos(theta + time)
	)
	return to_global(Vector3()) + dynamic_orbitals_amplitude * orbit_radius * (1 + 0.22*sin(time)) * curve


func get_expand_of(orbital):
	var direction = OctronLattice.DIRECTIONS[self.orbitals[orbital]]
	return to_global(direction)


func colorize_surface_for(orbital, color):
	var i = self.orbitals[orbital]
#	color = ColorN('aquamarine')
#	prints("SurfMat", self.mesh.get_surface_material(i))
	var colorized_material = SpatialMaterial.new()
	colorized_material.set_albedo(color)
	colorized_material.set_emission(color)
	colorized_material.emission_enabled = true
	self.mesh.set_surface_material(i, colorized_material)


func is_moving():
	return self.linear_velocity.length_squared() > 0.1


var last_contact

signal terrain_collided

func _on_OctronCharacter_body_entered(body):
#	print("contact with floor1")
	if body is OctronStaticBodyChunk :
#		print("contact with floor2")
		if is_moving():
			react_on_terain_contact()
		else:
			#print("Skipped \"invisible\" collision.")
			pass


func react_on_terain_contact():
	last_contact = OS.get_ticks_msec()
	emit_signal("terrain_collided")
	accelerate_orbitals_briefly()
#	controls.refill_jump_energy()
#	blink_outline_color()


func set_outline_color(color):
	mesh.octron_material.next_pass.set_shader_param("color", color)


func blink_outline_color():
	var blink_color = Color(0.0, 1.0, 0.0, 1.0)
	mesh.octron_material.next_pass.set_shader_param("color", blink_color)
	yield(get_tree().create_timer(0.300), "timeout")
	mesh.octron_material.next_pass.set_shader_param("color", Color(0.0,0,0,1.0))
	

var dynamic_orbitals_speed = 1.0
func accelerate_orbitals_briefly():
	dynamic_orbitals_speed = 1.55
	dynamic_orbitals_amplitude = 1.1
	yield(get_tree().create_timer(0.1), "timeout")
	dynamic_orbitals_amplitude = 1.0
	dynamic_orbitals_speed = 1.0
	

