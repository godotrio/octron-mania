extends Resource
class_name Octron

export(Color) var color := Color.white

# Identifier of the type of the octron (dirt, stone, …)
export(int) var primary_type_id = 0
export(int) var secondary_type_id = 0
export(int) var tertiary_type_id = 0
