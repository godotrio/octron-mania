extends OctronCharacterControls
class_name OctronKatamariControls

const ground_plane := Plane(Vector3.UP, 0)

export var super_jump_window:float = 128 #64  # ms
export var super_jump_window_before:float = 128  # ms
export var super_jump_strength_multiplier = 1.333
export var jump_strength = 10.0
export var jump_cooldown = 333  # ms


var strafe_strength = 23   # 15
var torque_strength = 18   # 13
var velocity_threshold = 15
var torque_threshold = 10


var _jump_energy := 0.0  # max 1.0
var _last_jump_intent_at := 0  # ms
var _last_jumped_at := 0  # ms
var _last_super_jumped_at := 0  # ms


func setup():
	.setup()
	self.character.connect("terrain_collided", self, "refill_jump_energy")


func process_controls(delta):
	.process_controls(delta)
	process_movement_inputs(delta)


func clear_movement_inputs():
#	self.character.linear_velocity *= 0.01
#	self.character.angular_velocity *= 0.01
	self.character.linear_velocity = Vector3.ZERO
	self.character.angular_velocity = Vector3.ZERO


func get_movement_axis_backward():
	# Projecting on the plane allows us to use the camera as referential
	# for controls but using only its "longitude" and ignoring its "latitude"
	# and "altitude". We don't want controls to be less effective when the camera
	# looks at the character slightly from above or below.
	var plane := Plane(Vector3.UP, 0)
	var cam_basis = self.camera.global_transform.basis
	var z = ground_plane.project(cam_basis.z).normalized()
	return z


func get_movement_axis_right():
	# Projecting on the plane allows us to use the camera as referential
	# for controls but using only its "longitude" and ignoring its "latitude"
	# and "altitude". We don't want controls to be less effective when the camera
	# looks at the character slightly from above or below.
	var plane := Plane(Vector3.UP, 0)
	var cam_basis = self.camera.global_transform.basis
	var x = ground_plane.project(cam_basis.x).normalized()
	return x


func get_movement_intent():
	var z = get_movement_axis_backward()
	var x = get_movement_axis_right()
	var intent = Vector3.ZERO
	if Input.is_action_pressed("move_forward"):
		intent += z * -1
	if Input.is_action_pressed("move_backward"):
		intent += z
	if Input.is_action_pressed("move_left"):
		intent += x * -1
	if Input.is_action_pressed("move_right"):
		intent += x
	intent = intent.normalized()
	return intent


func process_movement_inputs(delta):
	
#	var swerve_strength = 0.0055

	# Detect whether the camera is upside-down in world space.
	var headstand = self.camera.to_global(Vector3.UP)
	headstand -= self.camera.to_global(Vector3.ZERO)
	headstand = headstand.dot(Vector3.UP) < 0
	headstand = -1.0 if headstand else 1.0
	
#	print(self.camera.to_global(Vector3.UP).dot(Vector3.UP))
#	assert(headstand > 0)
	
	
	var z = get_movement_axis_backward()
	var x = get_movement_axis_right()
	# Short variables are sweet
	var vel = self.character.linear_velocity
	var ang = self.character.angular_velocity
	# Variables we're going to fill and use
	var velocity := Vector3.ZERO  # in world space
	var torque := Vector3.ZERO  # in world space
	var direction := Vector3.ZERO  # in world space
	var axis := Vector3.ZERO  # in world space
	#var dot := 0
	
	
	
	# Learn to use dot()
	# Here we use it to see if how much of the current velocity is aligned
	# with our direction vector, and if it's too much, we ignore the input.
	# The dot() of two vectors is highest when vectors are aligned
	# and lowest (in the negatives) when the vectors are opposite.
	# it is 0 when vectors are perpendicular (orthogonal), which is useful too.
	if Input.is_action_pressed("move_forward"):
		direction = z * -1
		if vel.dot(direction) < velocity_threshold:
			velocity += direction
		axis = x * -1 * headstand
		if ang.dot(axis) < torque_threshold:
			torque += axis
	if Input.is_action_pressed("move_backward"):
		direction = z
		if vel.dot(direction) < velocity_threshold:
			velocity += direction
		axis = x * headstand
		if ang.dot(axis) < torque_threshold:
			torque += axis
	if Input.is_action_pressed("move_left"):
		direction = x * -1
		if vel.dot(direction) < velocity_threshold:
			velocity += direction
		axis = z * headstand
		if ang.dot(axis) < torque_threshold:
			torque += axis
	if Input.is_action_pressed("move_right"):
		direction = x
		if vel.dot(direction) < velocity_threshold:
			velocity += direction
		axis = z * -1 * headstand
		if ang.dot(axis) < torque_threshold:
			torque += axis
	velocity = velocity.normalized() * strafe_strength * delta * 60.0
	torque = torque.normalized() * torque_strength * delta * 60.0

	# Hmmm... Buggy
#	dot = vel.dot(velocity)
#	if dot < 0:
#		velocity *= swerve_strength * dot * -1

	self.character.add_central_force(velocity)
	self.character.add_torque(torque)
	
	# Collect jump intent
	if Input.is_action_just_pressed("char_jump"):
		_last_jump_intent_at = get_now()
	
	# Add bounciness in the mix, somehow
	if Input.is_action_pressed("char_bounce"):
		self.character.become_bouncy()
	else:
		self.character.stop_being_bouncy()
	
	var is_super_jumping = false
	var now = get_now()
	if should_super_jump():
		if can_super_jump():
			do_super_jump()
			is_super_jumping = true
	if (not is_super_jumping) and Input.is_action_pressed("char_jump"):
		if not self.character.is_moving():
			print("Jumping because we are not moving…")
		if can_jump() or not self.character.is_moving():
			do_jump()


func do_jump():
	var now = get_now()
#	prints("JUMP!", now)
	var jump_vector = Vector3.UP
	jump_vector += get_movement_intent() * 0.04
	jump_vector = jump_vector.normalized()
	self.character.apply_central_impulse(jump_strength * jump_vector)
	_last_jumped_at = now
	_jump_energy = max(0.0, _jump_energy - 1.0)


func do_super_jump():
	var now = get_now()
#	prints("SUPER JUMP!", now)
	var jump_vector = Vector3.UP
	jump_vector += get_movement_intent() * 0.1
	jump_vector = jump_vector.normalized()
	self.character.apply_central_impulse(
		super_jump_strength_multiplier * jump_strength * jump_vector
	)
	_last_jumped_at = now
	_last_super_jumped_at = now
	_jump_energy = 0.0
	
	self.character.enhance_trail()
#	yield(self.character.get_tree().create_timer(1.618), 'timeout')


func refill_jump_energy():
	var now = get_now()
	if (now - _last_jumped_at) > jump_cooldown:
		_jump_energy = 1.0


func get_now():  # ms
	return OS.get_ticks_msec()


func can_jump():
	if 0.0 == _jump_energy:
		return false
	
	if self.character.is_falling():
		return false
	
	var now = get_now()
	if (now - _last_jumped_at) < jump_cooldown:
		return false
	return true


func can_super_jump():
	if not can_jump():
		return false
	
	if null == self.character.last_contact:
		return false
	
	var now = get_now()
	if (now - self.character.last_contact) < super_jump_window:
		return true
	else:
		return false


func should_super_jump():
	var now = get_now()
	if (now - _last_jump_intent_at) < super_jump_window_before:
		return true
	else:
		return false


func get_ms_since_last_super_jump():
	return get_now() - _last_super_jumped_at

