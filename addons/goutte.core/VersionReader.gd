class_name VersionReader


# You have to generate this by your own means.
# Example:
# $ git describe --tags > VERSION
const VERSION_FILEPATH = "res://VERSION"


static func get_version() -> Version:
	
	var version_string := "0.0.0"
	
	#version_string = get_version_string_from_git()
	if "0.0.0" == version_string:
		version_string = get_version_string_from_file()
	
	return Version.new(version_string)


static func get_version_string_from_file() -> String:
	
	var version_string := "0.0.0"
	
	var file = File.new()
	if OK == file.open(VERSION_FILEPATH, File.READ):
		version_string = file.get_as_text()
		version_string = version_string.strip_escapes()
		version_string = version_string.strip_edges()
	file.close()
	return version_string


#static func get_version_string_from_git() -> String:
#	var cmd = "git describe --tags"
#	…
