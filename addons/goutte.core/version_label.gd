extends Label


# Add this script to a Label displaying the version number.
# (todo) It will fetch the version from git tags.
# As a fallback, it will fetch the version from the VERSION file.


export var prefix := "v"
export var suffix := ""


func _ready():
	text = "%s%s%s" %  [
		prefix,
		str(VersionReader.get_version()),
		suffix,
	]
