extends Reference
class_name Version

# Holds a semantic version string.
# See VersionReader.
# Semantic Versioning https://semver.org/

# MUST Match
# ^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$
var _version := "0.0.0"


func _init(version:String):
	_version = version


func _to_string() -> String:
	return to_string()


func to_string() -> String:
	return _version


#func parse_version():
#func get_minor():
#func get_major():
#func get_patch():
#func get_prerelease():
#func get_build():
