extends AudioStreamPlayer3D
class_name AudioMultiStreamPlayer3D


# An AudioStreamPlayer3D that can play one of multiple audio streams.
# Useful to get a little variety in your sound effects.
# The `stream` property is overwritten, if you set it yourself
# it will not be part of the played sounds.


# Play the sounds at random, instead of in order.
#export(bool) var play_random := false

export(AudioStream) var audio_stream_00:AudioStream
export(AudioStream) var audio_stream_01:AudioStream
export(AudioStream) var audio_stream_02:AudioStream
export(AudioStream) var audio_stream_03:AudioStream
export(AudioStream) var audio_stream_04:AudioStream
export(AudioStream) var audio_stream_05:AudioStream

export(int) var audio_streams_count := 0


func play(from_position:=0.0) -> void:
	next_stream()
	.play(from_position)


var _current_stream_idx := -1


func next_stream():
	if audio_streams_count > 0:
		_current_stream_idx = (_current_stream_idx + 1) % audio_streams_count
		stream = get("audio_stream_%02d" % _current_stream_idx)

