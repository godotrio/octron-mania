extends Node


# Directory() does not work in standalone (exported builds).
# This is Godot's biggest derp yet.
# This class aims to help hack around this … _sigh_.


var is_logging := false
var _cache := ConfigFile.new()


func get_cache_path():
	return "res://finder.cfg"


func _init():
	var cache_path = get_cache_path()
	var err = _cache.load(cache_path)

	if err != OK:
		_print("No cache found at `%s'." % [cache_path])
		save_cache()


func _print(thing) -> void:
	if is_logging:
		print("[%s] %s" % [name, str(thing)])


func get_cache():
	return _cache


func save_cache():
	var cache_path = get_cache_path()
	var err = _cache.save(cache_path)
	if err != OK:
		printerr("[%s] ERROR! Cannot save cache at `%s'." % [name, cache_path])


func list_directory(directory_path:String) -> Dictionary:
	"""
	"""
	if is_in_editor():
		var files_paths = Array()
		var directories_paths = Array()
		
		var directory = Directory.new()
		if OK == directory.open(directory_path):
			directory.list_dir_begin()
			var file_name = directory.get_next()
			while file_name != "":
				var path = "%s/%s" % [directory_path, file_name]
				if directory.current_is_dir():
					_print("Found directory: `%s'." % path)
					if "." != file_name and ".." != file_name:
						directories_paths.append(path)
				else:
					_print("Found file: `%s'." % path)
					files_paths.append(path)
				
				file_name = directory.get_next()
		else:
			_print("Error when trying to open `%s'." % directory_path)
		
		_cache.set_value(directory_path, "directories_all", directories_paths)
		_cache.set_value(directory_path, "files_all", files_paths)
		save_cache()
	
	return {
		'files': _cache.get_value(directory_path, "files_all", Array()),
		'directories': _cache.get_value(directory_path, "directories_all", Array()),
	}


func list_files_with_extensions(directory_path:String, extensions:Array) -> Array:
	var directory_lists = list_directory(directory_path)
	var all_files = directory_lists['files']
	var found_files = Array()
	for file in all_files:
		for extension in extensions:
			if file.ends_with(extension):
				found_files.append(file)
	
	return found_files


func is_in_editor() -> bool:
	return not OS.has_feature("standalone")
