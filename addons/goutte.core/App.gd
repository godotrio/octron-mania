extends Node


# Handles Application logic.
# Stuff we'll need in every game.
# - Closing
# - Fullscreen Toggle
# - Options
# - …
# Meant to be used as Godot Singleton,
# usually with the name `App`.


func quit() -> void:
	get_tree().quit()


func exit() -> void:
	quit()


func toggle_fullscreen():
	OS.window_fullscreen = not OS.window_fullscreen


signal option_changed


const OPTIONS_FILEPATH := "user://options.cfg"

# Add more using add_options_meta() or just edit those :(|)
var _options_meta : Dictionary = {
	"audio": {
		"muted": {
			"type": "toggle", # or "checkbox"
			"default": false,
		},
		"main_volume": {
			"type": "horizontal_slider",
			"minimum": 0,
			"maximum": 100,
			"step": 1,
			"default": 80,
		},
	},
}
var _options:ConfigFile


func _init():
	_load_options()
	_apply_known_options()


func _load_options():
	_options = ConfigFile.new()
	var loading = _options.load(OPTIONS_FILEPATH)
	if OK == loading:
		for section in _options_meta:
			for key in _options_meta[section]:
				if not _options.has_section_key(section, key):
					_options.set_value(section, key, _options_meta[section][key]["default"])
	else:
		for section in _options_meta:
			for key in _options_meta[section]:
				_options.set_value(section, key, _options_meta[section][key]["default"])
		_options.save(OPTIONS_FILEPATH)


func add_options_meta(options_meta:Dictionary):
	merge_dict(_options_meta, options_meta)
	_load_options()
	_apply_known_options()


func _apply_known_options():
	adjust_main_volume_from_options()


# https://godotengine.org/qa/8024/update-dictionary-method
static func merge_dict(dest, source):
	for key in source:                     # go via all keys in source
		if dest.has(key):                  # we found matching key in dest
			var dest_value = dest[key]     # get value
			var source_value = source[key] # get value in the source dict
			if typeof(dest_value) == TYPE_DICTIONARY:
				if typeof(source_value) == TYPE_DICTIONARY:
					merge_dict(dest_value, source_value)
				else:
					dest[key] = source_value # override the dest value
			else:
				dest[key] = source_value     # add to dictionary
		else:
			dest[key] = source[key]          # just add value to the dest


func set_option(option_section, option_key, option_value) -> void:
	self._options.set_value(option_section, option_key, option_value)
	_options.save(OPTIONS_FILEPATH)
	emit_signal("option_changed", option_section, option_key, option_value)


func get_option(option_section, option_key, default_value=null):
	return self._options.get_value(option_section, option_key, default_value)


func get_unique_device_id():
	return OS.get_unique_id()


func adjust_main_volume_from_options():
	prints("volume", get_option('audio', 'main_volume', 100))
	var db_volume = option_to_db(get_option('audio', 'main_volume', 100))
	AudioServer.set_bus_volume_db(
		AudioServer.get_bus_index("Master"),
		db_volume
	)


static func option_to_db(option_value):
	return option_value * 0.6 - 60.0



