

enum {
	FORMAT_UNKNOWN,
	FORMAT_INI, # todo
	FORMAT_JSON,
	FORMAT_YAML, # todo
}


static func load_from_files(filepaths:Array):
	assert(filepaths is Array)
	assert(not filepaths.empty())
	
	var data = Dictionary()
	
	for filepath in filepaths:
		var format = detect_format(filepath)
		
		var file_data = Dictionary()
		if FORMAT_JSON == format:
			var file = File.new()
			var can_open = file.open(filepath, File.READ)
			if OK != can_open:
				printerr("Config file `%s' cannot be read from." % filepath)
				continue
			var file_contents = file.get_as_text()
			file_data = JSON.parse(file_contents)
			if OK != file_data.error:
				printerr("Config file `%s' cannot be read as JSON." % filepath)
				printerr(file_data.error_string)
				printerr("on line " + str(file_data.error_line) + " of")
				printerr(file_contents)
				continue
			merge_dict(data, file_data.result)
#		elif FORMAT_INI == format:
#		elif FORMAT_YAML == format:
		else:
			assert(not "supported yet")
	
	return data


static func detect_format(filepath:String):
	if filepath.ends_with('.dist'):
		filepath = filepath.substr(0, filepath.length() - 5)
	if filepath.ends_with('.ini'):
		return FORMAT_INI
	if filepath.ends_with('.cfg'):
		return FORMAT_INI
	if filepath.ends_with('.json'):
		return FORMAT_JSON
	if filepath.ends_with('.yml'):
		return FORMAT_YAML
	if filepath.ends_with('.yaml'):
		return FORMAT_YAML
	return FORMAT_UNKNOWN


# https://godotengine.org/qa/8024/update-dictionary-method
static func merge_dict(dest, source):
	for key in source:                     # go via all keys in source
		if dest.has(key):                  # we found matching key in dest
			var dest_value = dest[key]     # get value 
			var source_value = source[key] # get value in the source dict           
			if typeof(dest_value) == TYPE_DICTIONARY:       
				if typeof(source_value) == TYPE_DICTIONARY: 
					merge_dict(dest_value, source_value)  
				else:
					dest[key] = source_value # override the dest value
			else:
				dest[key] = source_value     # add to dictionary 
		else:
			dest[key] = source[key]          # just add value to the dest
